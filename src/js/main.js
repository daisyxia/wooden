//alert
import swal from 'sweetalert';

//Dropdowns
import "bootstrap/dist/js/bootstrap.min.js";
import "@popperjs/core/dist/umd/popper.min.js";

//burger
import 'zeynepjs';

$(function() {

  var zeynep = $('.zeynep').zeynep({
    opened: function () {
      console.log('the side menu is opened')
      $('body').addClass('is-open')
    }
  })
  
  // dynamically bind 'closing' event
  zeynep.on('closing', function () {
    console.log('this event is dynamically binded')
    $('body').removeClass('is-open')
  })

  // handle zeynepjs overlay click
  $('.zeynep-overlay').on('click', function () {
    zeynep.close()
  })

  // open zeynepjs side menu
  $('.btn-open').on('click', function (e) {
    e.preventDefault()
    zeynep.open()
  })

})

//date
import flatpickr from "flatpickr/dist/flatpickr";

const fp = flatpickr("#time", {
  dateFormat: 'y/m/d',
});


//Carousel
import Swiper from 'swiper';
//首頁大圖輪播
const swiperInBanner = new Swiper('.swiper-container.index-banner-swiper', {
    slidesPerView: 1, //- 顯示一塊
    breakpointsInverse:false,
    observer:true,
    observeParents:true,
    navigation: { //箭頭
        nextEl: '.decorationArrow1',
    },
    pagination: {
        el: ".swiper-pagination",
        clickable: true,
    }
});

//index熱賣商品及最近瀏覽商品
const swiperInHotProduct = new Swiper('.swiper-container.sixPcs-swiper', {
    navigation: { //箭頭
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
    breakpointsInverse:false,
    observer:true,
    observeParents:true,
    breakpoints: {
        1200: {
            slidesPerView: 6,
            spaceBetween: 30,
        },
        992: {
            slidesPerView: 2,
            spaceBetween: 30,
        },
        360: {
            slidesPerView: 2,
            spaceBetween: 30,
        },
        320: {
            slidesPerView: 2,
            spaceBetween: 30,
        },
        280: {
            slidesPerView: 1,
            spaceBetween: 30,
        },

    }
});

//商品頁手機版點擊購物車
const phoneProductImg = new Swiper('.swiper-container.phoneProductImg-swiper', {
    slidesPerView: 1, //- 顯示一塊
    breakpointsInverse:false,
    observer:true,
    observeParents:true,
    navigation: { //箭頭
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
    pagination: {
        el: ".swiper-pagination",
        clickable: true,
    }
});


//if like color for red
let heart = document.getElementsByClassName('likeIcon');
Object.keys(heart).forEach((key) => {
    let target = heart[key];
    target.addEventListener('click', () => {
        swal("你已加入收藏", "Add to Favorites thank!!", "success");
        ;
    })
})

let cartIcon = document.getElementsByClassName('cartIcon');
Object.keys(cartIcon).forEach((key) => {
    let target = cartIcon[key];
    target.addEventListener('click', () => {
        swal("你已加入購物車", "Add to cart thank!!", "success");
        ;
    })
})


let phoneLikeIcon = document.getElementsByClassName('addlikebtn');
Object.keys(phoneLikeIcon).forEach((key) => {
    let target = phoneLikeIcon[key];
    target.addEventListener('click', () => {
        target.classList.toggle("bi-suit-heart");
        target.classList.toggle("bi-suit-heart-fill");
        if(target.classList.contains('bi-suit-heart-fill')){
            swal("你已加入收藏", "Add to Favorites thank!!", "success");
        }else{
            swal("你已取消收藏", "Cancel to Favorites thank!!", "warning");
        }
    })
})



